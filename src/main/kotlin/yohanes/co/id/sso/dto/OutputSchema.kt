package yohanes.co.id.sso.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

class OutputSchema<X, Y> (
        @JsonProperty("code") val code: BigDecimal,
        @JsonProperty("status") val status: String,
        @JsonProperty("message") val message: String,
        @JsonProperty("data") val data: X,
        @JsonProperty("errors") val errors: Y,
        @JsonProperty("path") val path: String
)