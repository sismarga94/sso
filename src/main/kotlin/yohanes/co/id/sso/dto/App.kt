package yohanes.co.id.sso.dto

import java.sql.Time

class App (
) {
        var baseUrl: String? = null
        var apiVersion: String? = null
        var pageStartTime: Time? = null
        var now: String? = null
        var clientBuild: String? = null
        var clientVersion: String? = null
        var deviceModel: String? = null
        var osVersion: String? = null
        var appId: String? = null
        var defaultRole: String? = null
        var userId: String? = null
        var roleId: String? = null
        var officeCode: String? = null
        var localize: String? = null
        var log: LogRequest? = null
        var response: OutputSchema<Any, Any>? = null
}