package yohanes.co.id.sso.dto

abstract class DataHolder {
    abstract val app: Map<String, String>

    abstract fun loadObject(text: String): String
}
