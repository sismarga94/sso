package yohanes.co.id.sso.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class LogoutDto (
    @JsonProperty("token") val token: String,
    @JsonProperty("user_id") val userId: String?
)