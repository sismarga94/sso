package yohanes.co.id.sso.dto

import yohanes.co.id.sso.model.log.Log
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

class LogRequest (
    @JsonProperty("request_direction") val direction: String,
    @JsonProperty("request_url") val url: String,
    @JsonProperty("request_method") val method: String,
    @JsonProperty("request_header") val header: String,
    @JsonProperty("request_body") val reqBody: String,
    @JsonProperty("response_body") val respBody: String,
    @JsonProperty("response_error") val respError: String,
    @JsonProperty("app_id") val appId: String,
    @JsonProperty("time_load") val timeLoad: Long,
    @JsonProperty("user_id") val userId: String,
    @JsonProperty("created_at") val createdAt: LocalDateTime,
    @JsonProperty("ip_address") val ipAddress: String
)

fun toLog(){
    Log()
}