package yohanes.co.id.sso.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class LoginDto (
    @JsonProperty("email") val email: String,
    @JsonProperty("password") val password: String,
    @JsonProperty("device_id") val deviceId: String,
    @JsonProperty("device_type") val deviceType: String? = null
)