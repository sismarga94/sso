package yohanes.co.id.sso.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class ChangePwdDto (
        @JsonProperty("old_password") val oldPassword: String,
        @JsonProperty("password") val password: String,
        @JsonProperty("confirm_password") val confirmPassword: String,
        @JsonProperty("device_id") val deviceId: String
)