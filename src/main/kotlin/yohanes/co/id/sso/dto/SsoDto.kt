package yohanes.co.id.sso.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class SsoDto (
        @JsonProperty("app_token") val appToken: String
)