package yohanes.co.id.sso

import yohanes.co.id.sso.filter.LogDispatcherServlet
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.ServletRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.web.client.RestTemplate
import org.springframework.web.servlet.DispatcherServlet
import org.springframework.web.servlet.config.annotation.CorsRegistry

import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@SpringBootApplication
class SsoApplication {
	@Bean
	fun getRestTemplate(): RestTemplate {
		return RestTemplate()
	}

	@Bean
	open fun dispatcherRegistration(): ServletRegistrationBean<*> {
		return ServletRegistrationBean(dispatcherServlet())
	}

	@Bean(name = [DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME])
	open fun dispatcherServlet(): DispatcherServlet {
		return LogDispatcherServlet()
	}

	@Bean
	fun corsConfigurer(): WebMvcConfigurer? {
		return object : WebMvcConfigurer {
			override fun addCorsMappings(registry: CorsRegistry) {
				registry.addMapping("/**").allowedOrigins("*")
			}
		}
	}
}

fun main(args: Array<String>) {
	runApplication<SsoApplication>(*args)
}

