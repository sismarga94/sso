package yohanes.co.id.sso.controller

import yohanes.co.id.sso.dto.OutputSchema
import yohanes.co.id.sso.model.apuppt.Menu
import yohanes.co.id.sso.repository.SsoRepository
import yohanes.co.id.sso.repository.apuppt.MenuRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.servlet.http.HttpSession

@RestController
class SystemController @Autowired constructor(
        val menuRepository: MenuRepository,
        val ssoRepository: SsoRepository,
        val session: HttpSession,
){
    @GetMapping("/systemtime")
    fun getSystemTime(): String {
        return LocalDateTime.now().toString()
    }
    @GetMapping("/user_profile")
    fun getUserProfile(@RequestHeader("Token") token: String,
                       @RequestHeader("App-Token") appToken: String
    ): Map<String, Any?>{
        return ssoRepository.getProfile(token, appToken)
    }
    @GetMapping("/get_html")
    fun getList(): OutputSchema<List<Menu>, Map<String, Any>> {
        val x = menuRepository.findAll()
        return OutputSchema(
                BigDecimal(200), "success", "success", x, emptyMap(), "url"
        )
    }
}