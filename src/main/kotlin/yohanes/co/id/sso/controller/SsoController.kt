package yohanes.co.id.sso.controller

import yohanes.co.id.sso.dto.ChangePwdDto
import yohanes.co.id.sso.dto.LoginDto
import yohanes.co.id.sso.dto.LogoutDto
import yohanes.co.id.sso.dto.SsoDto
import yohanes.co.id.sso.repository.SsoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import kotlin.math.log

@RestController
class SsoController @Autowired constructor(
    val ssoRepository: SsoRepository
){
    @PostMapping("/sso")
    fun checkSso(@RequestBody ssoDto: SsoDto): Map<String, Any?> {
        return ssoRepository.checkAppToken(ssoDto.appToken)
    }
    @PostMapping("/login")
    fun login(@RequestBody loginDto: LoginDto): Map<String, Any?> {
        return ssoRepository.login(loginDto)
    }
    @PostMapping("/logout")
    fun logout(logoutDto: LogoutDto): Map<String, Any?> {
        return ssoRepository.logout(logoutDto)
    }
    @PostMapping("/change_password")
    fun changePassword(@RequestHeader("Token") token: String,
                       @RequestHeader("App-Token") appToken: String,
                       changePwdDto: ChangePwdDto
    ): Map<String, Any?>{
        return ssoRepository.changePassword(token, appToken, changePwdDto)
    }
}