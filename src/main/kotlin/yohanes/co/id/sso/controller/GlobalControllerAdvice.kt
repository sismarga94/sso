package yohanes.co.id.sso.controller

import yohanes.co.id.sso.dto.OutputSchema
import yohanes.co.id.sso.util.InternalServerException
import yohanes.co.id.sso.util.TokenUnauthorizedException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import javax.servlet.http.HttpServletResponse

@ControllerAdvice
class GlobalControllerAdvice {
    @ExceptionHandler(TokenUnauthorizedException::class)
    @ResponseBody
    fun returnUnauthorizedException(response: HttpServletResponse, exception: TokenUnauthorizedException): OutputSchema<String, String?> {
        response.status = HttpStatus.UNAUTHORIZED.value()
        return OutputSchema(response.status.toBigDecimal(), "fail", "",
                exception.msg, null, exception.path)
    }

    @ExceptionHandler(InternalServerException::class)
    @ResponseBody
    fun returnInternalServerException(response: HttpServletResponse, exception: InternalServerException): OutputSchema<String, String?> {
        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
        return OutputSchema(response.status.toBigDecimal(), "fail", "",
                exception.msg, null, exception.path)
    }
}