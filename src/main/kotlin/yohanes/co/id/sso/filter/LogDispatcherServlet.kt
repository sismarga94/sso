package yohanes.co.id.sso.filter

import yohanes.co.id.sso.model.log.Log
import yohanes.co.id.sso.repository.log.LogRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.servlet.DispatcherServlet
import org.springframework.web.util.ContentCachingRequestWrapper
import org.springframework.web.util.ContentCachingResponseWrapper
import org.springframework.web.util.WebUtils
import org.json.JSONObject
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.time.LocalDateTime
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class LogDispatcherServlet: DispatcherServlet() {
    @Autowired
    lateinit var logRepository: LogRepository
    @Autowired
    lateinit var objectMapper: ObjectMapper

    override fun doDispatch(request: HttpServletRequest, response: HttpServletResponse) {
        val ssoData = request.getAttribute("ssoData") as Map<String, Any?>?
        var newRequest = request
        var newResponse = response
        if (request !is ContentCachingRequestWrapper) {
            newRequest = ContentCachingRequestWrapper(request)
        }
        if (response !is ContentCachingResponseWrapper) {
            newResponse = ContentCachingResponseWrapper(response)
        }
        val handler = getHandler(newRequest)
        val startTime = System.currentTimeMillis()
        var endTime = System.currentTimeMillis()
        try {
            super.doDispatch(newRequest, newResponse)
            endTime = System.currentTimeMillis()
        } catch (ex: Exception) {

        } finally {
            val jsonRequest = JSONObject(getRequestPayload(newRequest))
            var hashtable: Hashtable<String, Any?> = Hashtable()
            
            val log = Log()
            log.request_direction = "IN"
            log.request_url = request.requestURI
            log.request_method = request.method
            log.request_header = objectMapper.writeValueAsString(getHeaderPayload(newRequest))
            log.request_body = getRequestPayload(newRequest)
            log.response_body = getResponsePayload(newResponse)
            log.response_error = getResponsePayload(newResponse)
            log.app_id = "APP_ID"
            log.time_load = (System.currentTimeMillis() - (request.getAttribute("startTime") as Long))/1000
            log.user_id = (ssoData?.get("user_id") as String?)?:jsonRequest["email"] as String?: ""
            log.created_at = LocalDateTime.now()
            log.ip_address = request.localAddr

            if(log.request_url != "/error") {
                logRepository.save(log)
            }
            updateResponse(newResponse)
        }
    }

    private fun getHeaderPayload(request: HttpServletRequest): Map<String, String> {
        val headers = listOfNotNull(
                if(request.getHeader("app-token")!=null) "app_token" to request.getHeader("app-token") else null,
                if(request.getHeader("token")!=null) "token" to request.getHeader("token") else null,
                if(request.getHeader("Content-Type")!=null) "Content-Type" to request.getHeader("Content-Type") else null
        ).toMap()
        return headers
    }

    fun getRequestPayload(request: HttpServletRequest): String{
        val wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper::class.java)
        if (wrapper != null) {
            val buf = wrapper.contentAsByteArray

            if (buf.isNotEmpty())
            {
                val length = Math.min(buf.size, 5120)
                try {
                    return String(buf, 0, length, Charset.forName(wrapper.characterEncoding))
                } catch (ex: UnsupportedEncodingException) {
                    
                }
            }
        }
        return "<empty>"
    }

    private fun getResponsePayload(response: HttpServletResponse): String {
        val wrapper = WebUtils.getNativeResponse(response, ContentCachingResponseWrapper::class.java)
        if (wrapper != null) {

            val buf = wrapper.contentAsByteArray
            if (buf.isNotEmpty()) {
                val length = Math.min(buf.size, 5120)
                try {
                    return String(buf, 0, length, Charset.forName(wrapper.characterEncoding))
                } catch (ex: UnsupportedEncodingException) {
                    
                }
            }
        }
        return "<empty>"
    }

    @Throws(IOException::class)
    private fun updateResponse(response: HttpServletResponse) {
        val responseWrapper = WebUtils.getNativeResponse(response, ContentCachingResponseWrapper::class.java)
        responseWrapper!!.copyBodyToResponse()
    }
}