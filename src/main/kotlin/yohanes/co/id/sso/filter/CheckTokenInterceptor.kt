package yohanes.co.id.sso.filter

import yohanes.co.id.sso.model.log.Log
import yohanes.co.id.sso.repository.SsoRepository
import yohanes.co.id.sso.repository.log.LogRepository
import yohanes.co.id.sso.util.InternalServerException
import yohanes.co.id.sso.util.TokenUnauthorizedException
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.util.ContentCachingRequestWrapper
import org.springframework.web.util.ContentCachingResponseWrapper
import org.springframework.web.util.WebUtils
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.time.LocalDateTime
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession


@Component
class CheckTokenInterceptor: HandlerInterceptor {
    @Autowired
    lateinit var ssoRepository: SsoRepository
    @Autowired
    lateinit var logRepository: LogRepository
    @Autowired
    lateinit var objectMapper: ObjectMapper

    override fun preHandle(req: HttpServletRequest, res: HttpServletResponse, handler: Any): Boolean {
        val token = req.getHeader("Token")
        val startTime = System.currentTimeMillis()
        req.setAttribute("startTime", startTime)
        if(req.requestURI == "/sso" || req.requestURI == "/login" || req.requestURI == "/logout"
                || req.requestURI == "/error" || req.method == RequestMethod.OPTIONS.toString()) {

        } else {

            try {
                val resp = ssoRepository.checkToken(token)
                if (resp["status"] != "success") {
                    throw TokenUnauthorizedException("Token Not Found", req.requestURI)
                }
                req.setAttribute("ssoData", resp["data"] as Map<String, Any?>)
                addToCheckAuthModel(req.session, resp)
            } catch (exception: Exception){
                throw InternalServerException("Internal Server Error", req.requestURI)
            }
        }
        return true
    }

        fun addToCheckAuthModel(session: HttpSession, map: Map<String, Any?>){
        val data = map["data"] as Map<String, Any?>?
        if(data != null){
            session.setAttribute("appId", data["app_id"])
            session.setAttribute("userId", data["user_id"])
            session.setAttribute("roleId", data["role_id"])
            session.setAttribute("officeCode", data["office_code"])
        }
    }
}