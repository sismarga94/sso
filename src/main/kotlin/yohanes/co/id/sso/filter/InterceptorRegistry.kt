package yohanes.co.id.sso.filter

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

@Component
class InterceptorRegistry: WebMvcConfigurerAdapter()
{
    @Autowired
    lateinit var checkTokenInterceptor: CheckTokenInterceptor

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(checkTokenInterceptor)
    }
}