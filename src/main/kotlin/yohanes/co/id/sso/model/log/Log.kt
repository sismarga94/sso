package yohanes.co.id.sso.model.log

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "log_request")
class Log () {
        @Id
        @Column(name="id")
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        var id: Long? = null
        var request_direction: String? = null
        var request_url: String? = null
        var request_method: String? = null
        var request_header: String? = null
        var request_body: String? = null
        var response_body: String? = null
        var response_error: String? = null
        var app_id: String? = null
        var time_load: Long? = null
        var user_id: String? = null
        var created_at: LocalDateTime? = null
        var ip_address: String? = null

}