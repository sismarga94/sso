package yohanes.co.id.sso.model.log

import com.zaxxer.hikari.HikariDataSource
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.transaction.PlatformTransactionManager
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource


@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "logEntityManagerFactory",
        transactionManagerRef = "logTransactionManager",
        basePackages = [
            "yohanes.co.id.sso.repository.log"
        ]
)
class LogConfig {
    @Bean(name = ["secondaryDataSourceProperties"])
    @ConfigurationProperties("spring.second.datasource")
    fun dataSourceProperties2(): DataSourceProperties {
        return DataSourceProperties()
    }
    @Bean(name = ["secondaryDataSource"])
    @ConfigurationProperties("spring.second.datasource.configuration")
    fun dataSource2(
            @Qualifier("secondaryDataSourceProperties") secondaryDataSourceProperties: DataSourceProperties
    ): DataSource {
        return secondaryDataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource::class.java).build()
    }

    @Bean(name = ["logEntityManagerFactory"])
    fun logEntityManagerFactory(
            secondaryEntityManagerFactoryBuilder: EntityManagerFactoryBuilder,
            @Qualifier("secondaryDataSource") secondaryDataSource: DataSource
    ): LocalContainerEntityManagerFactoryBean {
        return secondaryEntityManagerFactoryBuilder
                .dataSource(secondaryDataSource)
                .packages("yohanes.co.id.sso.model.log")
                .build()
    }


    @Bean(name = ["logTransactionManager"])
    fun logTransactionManager(
            @Qualifier("logEntityManagerFactory") secondaryEntityManagerFactory: EntityManagerFactory
    ): PlatformTransactionManager? {
        return JpaTransactionManager(secondaryEntityManagerFactory)
    }

}