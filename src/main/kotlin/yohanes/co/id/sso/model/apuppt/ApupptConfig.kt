package yohanes.co.id.sso.model.apuppt

import com.zaxxer.hikari.HikariDataSource
import org.springframework.beans.factory.annotation.Qualifier

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "apupptEntityManagerFactory",
        transactionManagerRef = "apupptTransactionManager",
        basePackages = [
            "yohanes.co.id.sso.repository.apuppt"
        ]
)
class ApupptConfig {

    @Bean(name = ["primaryDataSourceProperties"])
    @Primary
    @ConfigurationProperties("spring.first.datasource")
    fun dataSourceProperties(): DataSourceProperties {
        return DataSourceProperties()
    }
    @Bean(name = ["primaryDataSource"])
    @Primary
    @ConfigurationProperties("spring.first.datasource.configuration")
    fun dataSource(
            @Qualifier("primaryDataSourceProperties") primaryDataSourceProperties: DataSourceProperties
    ): DataSource {
        return primaryDataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource::class.java).build()
    }

    @Bean(name = ["apupptEntityManagerFactory"])
    @Primary
    fun apupptEntityManagerFactory(
            primaryEntityManagerFactoryBuilder: EntityManagerFactoryBuilder,
            @Qualifier("primaryDataSource") primaryDataSource: DataSource
    ): LocalContainerEntityManagerFactoryBean {
        return primaryEntityManagerFactoryBuilder
                .dataSource(primaryDataSource)
                .packages("yohanes.co.id.sso.model.apuppt")
                .build()
    }


    @Bean(name = ["apupptTransactionManager"])
    @Primary
    fun apupptTransactionManager(
            @Qualifier("apupptEntityManagerFactory") primaryEntityManagerFactory: EntityManagerFactory
    ): PlatformTransactionManager? {
        return JpaTransactionManager(primaryEntityManagerFactory)
    }

}