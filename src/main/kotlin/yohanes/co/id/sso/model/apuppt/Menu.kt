package yohanes.co.id.sso.model.apuppt

import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "webmenu")
data class Menu (
        @Id
        val menu_id: String? = null,
        val parent_menu_id: String? = null,
        val menu_label: String? = null,
        val icon: String? = null,
        val url: String? = null,
        val order_num: BigDecimal? = BigDecimal.ZERO,
        val status: BigDecimal? = BigDecimal.ZERO,
        val created_at: LocalDateTime? = LocalDateTime.now(),
        val updated_at: LocalDateTime? = LocalDateTime.now(),
        val updated_by: String? = null
) {

}