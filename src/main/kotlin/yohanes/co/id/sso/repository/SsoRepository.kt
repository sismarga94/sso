package yohanes.co.id.sso.repository

import yohanes.co.id.sso.dto.ChangePwdDto
import yohanes.co.id.sso.dto.LoginDto
import yohanes.co.id.sso.dto.LogoutDto
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate
import net.minidev.json.JSONObject
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import org.springframework.stereotype.Component

@Component
class SsoRepository @Autowired constructor(
        val restTemplate: RestTemplate
){
    @Autowired lateinit var objectMapper: ObjectMapper
    val appToken = "appToken"

    fun checkAppToken (appToken1: String): Map<String, Any?>{
        val url = "http://1.2.3.4/sso/auth/check_app_token"
        val headers = HttpHeaders()
        headers.set("Accept", MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        headers.set("app-token", appToken1)

        val map: MultiValueMap<String, String> = LinkedMultiValueMap()
        map.add("app_token", appToken1)
        val requestEntity: HttpEntity<MultiValueMap<String, String>> = HttpEntity(map, headers)
        return try {
            val result: ResponseEntity<JSONObject> = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    requestEntity,
                    object : ParameterizedTypeReference<JSONObject>() {})
            result.body as Map<String, Any?>
        } catch (restClientException: RestClientException){
            throw restClientException
        }
    }

    fun login (loginDto: LoginDto): Map<String, Any?>{
        val url = "http://1.2.3.4/sso/auth/user_login"
        val headers = HttpHeaders()
        headers.set("Accept", MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        headers.set("app-token", appToken)
        val map: MultiValueMap<String, String> = LinkedMultiValueMap()
        map.add("email", loginDto.email)
        map.add("password", loginDto.password)
        map.add("device_id", loginDto.deviceId)
        val requestEntity: HttpEntity<MultiValueMap<String, String>> = HttpEntity(map, headers)
        return try {
            val result: ResponseEntity<JSONObject> = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    requestEntity,
                    object : ParameterizedTypeReference<JSONObject>() {})
            result.body as Map<String, Any?>
        } catch (restClientException: RestClientException){
            println("exception")
            throw restClientException
        }
    }

    fun logout(logoutDto: LogoutDto): Map<String, Any?> {
        val url = "http://1.2.3.4/sso/auth/logout"
        val headers = HttpHeaders()
        headers.set("Accept", MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        headers.set("app-token", appToken)

        val map: MultiValueMap<String, String> = LinkedMultiValueMap()
        map.add("token",logoutDto.token)
        val requestEntity: HttpEntity<MultiValueMap<String, String>> = HttpEntity(map, headers)
        return try {
            val result: ResponseEntity<JSONObject> = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    requestEntity,
                    object : ParameterizedTypeReference<JSONObject>() {})
            val body = result.body as Map<String, Any?>
            result.body as Map<String, Any?>
        } catch (restClientException: RestClientException){
            throw restClientException
        }
    }

    fun checkToken(token: String?): Map<String, Any?> {
        val url = "http://1.2.3.4/sso/auth/check_token"
        val headers = HttpHeaders()
        headers.set("Accept", MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        headers.set("app-token", appToken)
        headers.set("Token", token)
        val map: MultiValueMap<String, String> = LinkedMultiValueMap()
        map.add("token", token)
        val requestEntity: HttpEntity<MultiValueMap<String, String>> = HttpEntity(map, headers)
        return try {
            val result: ResponseEntity<JSONObject> = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    requestEntity,
                    object : ParameterizedTypeReference<JSONObject>() {})
            val body = result.body as Map<String, Any?>
            result.body as Map<String, Any?>
        } catch (restClientException: RestClientException){
            throw restClientException
        }
    }

    fun getProfile(token: String, appToken: String): Map<String, Any?>{
        val url = "http://1.2.3.4/sso/user/my_profile"
        val headers = HttpHeaders()
        headers.set("Accept", MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        headers.set("app-token", appToken)
        headers.set("token", token)
        val map: MultiValueMap<String, String> = LinkedMultiValueMap()
        map.add("token", token)
        val requestEntity: HttpEntity<MultiValueMap<String, String>> = HttpEntity(map, headers)
        return try {
            val result: ResponseEntity<JSONObject> = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    requestEntity,
                    object : ParameterizedTypeReference<JSONObject>() {})

            result.body as Map<String, Any?>
        } catch (restClientException: RestClientException){
            throw restClientException
        }
    }

    fun changePassword(token: String, appToken: String, changePwdDto: ChangePwdDto): Map<String, Any?>{
        val url = "http://1.2.3.4/sso/user/change_password"
        val headers = HttpHeaders()
        headers.set("Accept", MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        headers.set("app-token", appToken)
        headers.set("token", token)
        val map: MultiValueMap<String, String> = LinkedMultiValueMap()
        map.add("old_password", changePwdDto.oldPassword)
        map.add("password", changePwdDto.password)
        map.add("confirm_password", changePwdDto.confirmPassword)
        map.add("device_id", changePwdDto.deviceId)
        val requestEntity: HttpEntity<MultiValueMap<String, String>> = HttpEntity(map, headers)
        return try {
            val result: ResponseEntity<JSONObject> = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    requestEntity,
                    object : ParameterizedTypeReference<JSONObject>() {})

            result.body as Map<String, Any?>
        } catch (restClientException: RestClientException){
            throw restClientException
        }
    }
}