package yohanes.co.id.sso.repository.log

import yohanes.co.id.sso.model.log.Log
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LogRepository: JpaRepository<Log, Long> {
}