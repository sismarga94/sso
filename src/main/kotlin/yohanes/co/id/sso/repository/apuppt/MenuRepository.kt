package yohanes.co.id.sso.repository.apuppt

import yohanes.co.id.sso.model.apuppt.Menu
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MenuRepository: JpaRepository<Menu, String> {
}