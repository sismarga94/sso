package yohanes.co.id.sso.util

import java.lang.RuntimeException

data class InternalServerException (val msg: String, val path: String): RuntimeException(msg){
}