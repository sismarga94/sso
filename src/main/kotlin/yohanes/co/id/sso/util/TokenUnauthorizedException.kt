package yohanes.co.id.sso.util

import java.lang.RuntimeException

data class TokenUnauthorizedException (val msg: String, val path: String): RuntimeException(msg){
}